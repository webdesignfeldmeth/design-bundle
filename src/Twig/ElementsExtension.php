<?php

namespace DesignBundle\Twig;

use Aufwind\Stdlib\Html\Node;
use Twig\Extension\AbstractExtension;
use Aufwind\WebFeatureBundle\Feature;
use Twig\TwigFunction;
use Pimcore\Model\DataObject;

/**
 * LoaderExtension
 *
 * LICENSE: Copyright (c) 2020, Webdesign Feldmeth
 *
 * @category  DesignBundle
 *
 * @copyright 2020 Webdesign Feldmeth
 * @license   keine Lizenz angegeben
 *
 * @version   1.0.0
 *
 * @see       https://bitbucket.org/aufwindteam/aufwinddefault
 */
class ElementsExtension extends AbstractExtension
{
    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('get_gallery_or_slideshow_elements', function ($elements) {
                $galleryElements = [];
                foreach($elements as $element) {
                    // Wenn es einzelne Elemente sind
                    $galleryElements = $this->getElements($galleryElements, $element);

                    // Wenn es sich um Elemente innerhalb eines Ordners handelt
                    $galleryElements = $this->getFolderElements($galleryElements, $element);

                }

                return $galleryElements;
            }),

            new TwigFunction('get_unique_id', function() {
                $uniqueId = uniqid();
                return $uniqueId;
            })
        ];
    }

    protected function getElements($galleryElements, $element) {
        $elementTypes = ["folder"];
        if(!in_array($element->getType(), $elementTypes)) {
            $galleryElements[] = $element;
        }

        return $galleryElements;
    }

    protected function getFolderElements($galleryElements, $element) {
        if($element->getType() == "folder") {
            $folder = DataObject::getByPath($element->getFullPath());
            if(sizeof($folder->getChildren()) > 0) {
                foreach($folder->getChildren() as $elem) {
                    $galleryElements[] = $elem;
                }
            }
        }

        return $galleryElements;
    }
}
