<?php

namespace DesignBundle\Document\Areabrick\GoogleMap;

use DesignBundle\Document\Areabrick\AbstractAreabrick;
use DesignBundle\Model\Distance;
use DesignBundle\Model\RenderOption;
use Pimcore\Model\Document\Tag\Area\Info;

class DesignGoogleMap extends AbstractAreabrick
{
    /**
     * string.
     */
    protected $googleMapsHostUrl;

    /**
     * GoogleMap constructor.
     *
     * @param string $googleMapsHostUrl
     */
    public function __construct($googleMapsHostUrl = '')
    {
        $this->googleMapsHostUrl = $googleMapsHostUrl;
	}
	
	public function action(Info $info)
	{
		parent::action($info);
		$distance = new Distance();
		$renderOption = new RenderOption();

		$view = $info->getView();
		$view->distances = $distance->getDistances($this, $info);
		$view->googleMapsHostUrl = $this->googleMapsHostUrl;
	}

	public function getViewTemplate()
	{
		return "DesignBundle:Areas/designGoogleMap:view." . $this->getTemplateSuffix();
	}

    /**
     * @inheritDoc
     */
    public function getTemplateSuffix()
    {
        return static::TEMPLATE_SUFFIX_TWIG;
    }

	public function getName()
	{
		return "GoogleMap";
	}

	public function getDescription()
	{
		return "Design GoogleMap";
	}

	public function getGroupName(): ?string
	{
		return "Design";
	}

}
