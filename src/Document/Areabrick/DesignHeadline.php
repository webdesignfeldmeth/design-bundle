<?php

namespace DesignBundle\Document\Areabrick\Headline;

use DesignBundle\Document\Areabrick\AbstractAreabrick;
use DesignBundle\Model\Distance;
use DesignBundle\Model\RenderOption;
use Pimcore\Model\Document\Tag\Area\Info;

class DesignHeadline extends AbstractAreabrick
{
	public function action(Info $info)
	{
		parent::action($info);
		$distance = new Distance();
		$renderOption = new RenderOption();

		$anchorName = null;
        /** @var \Pimcore\Model\Document\Tag\Input $anchorNameElement */
		$anchorNameElement = $this->getDocumentTag($info->getDocument(), 'input', 'anchor_name');
		if (!$anchorNameElement->isEmpty()) {
            $anchorName = \Pimcore\File::getValidFilename($anchorNameElement->getData());
		}

		$view = $info->getView();
		$view->anchorName = $anchorName;
		$view->distances = $distance->getDistances($this, $info);
		$view->renderOptions = $renderOption->getRenderOptionClasses($this, $info);
	}

	public function getViewTemplate()
	{
		return "DesignBundle:Areas/designHeadline:view." . $this->getTemplateSuffix();
	}

    /**
     * @inheritDoc
     */
    public function getTemplateSuffix()
    {
        return static::TEMPLATE_SUFFIX_TWIG;
    }

	public function getName()
	{
		return "Headline";
	}

	public function getDescription()
	{
		return "Design Headline";
	}

	public function getGroupName(): ?string
	{
		return "Design";
	}

	public function getMargin($info) {
		$separator = " ";
		$marginTop = $this->getDocumentTag($info->getDocument(), 'select', 'margin_top');
		$marginRight = $this->getDocumentTag($info->getDocument(), 'select', 'margin_right');
		if($marginRight->text != null)
			$marginRight = $separator . $marginRight;
		$marginBottom = $this->getDocumentTag($info->getDocument(), 'select', 'margin_bottom');
		if($marginBottom->text != null)
			$marginBottom = $separator . $marginBottom;
		$marginLeft = $this->getDocumentTag($info->getDocument(), 'select', 'margin_left');
		if($marginLeft->text != null)
			$marginLeft = $separator . $marginLeft;

		return $marginTop . $marginRight . $marginBottom . $marginLeft;
	}

	public function getPadding($info) {
		$separator = " ";
		$paddingTop = $this->getDocumentTag($info->getDocument(), 'select', 'padding_top');
		$paddingRight = $this->getDocumentTag($info->getDocument(), 'select', 'padding_right');
		if($paddingRight->text != null)
			$paddingRight = $separator . $paddingRight;
		$paddingBottom = $this->getDocumentTag($info->getDocument(), 'select', 'padding_bottom');
		if($paddingBottom->text != null)
			$paddingBottom = $separator . $paddingBottom;
		$paddingLeft = $this->getDocumentTag($info->getDocument(), 'select', 'padding_left');
		if($paddingLeft->text != null)
			$paddingLeft = $separator . $paddingLeft;

		return $paddingTop . $paddingRight . $paddingBottom . $paddingLeft;
	}
}
