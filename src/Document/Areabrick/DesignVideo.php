<?php

namespace DesignBundle\Document\Areabrick\Video;

use DesignBundle\Document\Areabrick\AbstractAreabrick;
use DesignBundle\Model\Distance;
use DesignBundle\Model\RenderOption;
use Pimcore\Model\Document\Tag\Area\Info;

class DesignVideo extends AbstractAreabrick
{
	public function action(Info $info)
	{
		parent::action($info);
		$distance = new Distance();
		$renderOption = new RenderOption();

		/** @var \ToolboxBundle\Model\Document\Tag\Vhs $videoTag */
        $videoTag = $this->getDocumentTag($info->getDocument(), 'vhs', 'video');

        $videoParameter = $videoTag->getVideoParameter();

        $playInLightBox = $videoTag->getShowAsLightBox() === true ? 'true' : 'false';
        /** @var \Pimcore\Model\Document\Tag\Checkbox $autoPlayElement */
        $autoPlayElement = $this->getDocumentTag($info->getDocument(), 'checkbox', 'autoplay');
        // $autoPlay = $autoPlayElement->isChecked() === true && !$view->get('editmode');
        $videoType = $videoTag->getVideoType();
        $posterPath = null;
        $imageThumbnail = null;
        $poster = $videoTag->getPosterAsset();
        $videoId = $videoTag->id;

        if ($poster instanceof Asset\Image) {
            $imageThumbnail = $this->getConfigManager()->getImageThumbnailFromConfig('video_poster');
            $posterPath = $poster->getThumbnail($imageThumbnail);
        }

		$view = $info->getView();
		// $view->autoPlay = $autoPlay;
		// $view->posterPath= $posterPath;
		// $view->videoType = $videoType;
		// $view->playInLightbox = $playInLightBox;
		// $view->videoParameter = $videoParameter;
		// $view->videoId = $videoId;
		$view->distances = $distance->getDistances($this, $info);
		$view->renderOptions = $renderOption->getRenderOptionClasses($this, $info);
	}

	public function getViewTemplate()
	{
		return "DesignBundle:Areas/designVideo:view." . $this->getTemplateSuffix();
	}

    /**
     * @inheritDoc
     */
    public function getTemplateSuffix()
    {
        return static::TEMPLATE_SUFFIX_TWIG;
    }

	public function getName()
	{
		return "Video";
	}

	public function getDescription()
	{
		return "Design Video";
	}

	public function getGroupName(): ?string
	{
		return "Design";
	}
}
