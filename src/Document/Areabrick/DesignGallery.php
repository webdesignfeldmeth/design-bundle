<?php

namespace DesignBundle\Document\Areabrick\Gallery;

use DesignBundle\Document\Areabrick\AbstractAreabrick;
use DesignBundle\Model\Distance;
use DesignBundle\Model\RenderOption;
use Pimcore\Model\Document\Tag\Area\Info;

class DesignGallery extends AbstractAreabrick
{
	public function action(Info $info)
	{
		parent::action($info);
		$distance = new Distance();
		$renderOption = new RenderOption();
		$loading = $this->getDocumentTag($info->getDocument(), 'select', 'loading');

		$infoParams = $info->getParams();
        if (isset($infoParams['toolboxGalleryId'])) {
            $id = $infoParams['toolboxGalleryId'];
        } else {
            $id = uniqid('gallery-');
        }

        /** @var \Pimcore\Model\Document\Tag\Relations $imagesField */
        $imagesField = $this->getDocumentTag($info->getDocument(), 'relations', 'images');

        $adaptiveHeight = $this->getDocumentTag($info->getDocument(), 'checkbox', 'adaptive_height');
        $autoplay = $this->getDocumentTag($info->getDocument(), 'checkbox', 'autoplay');
        $dots = $this->getDocumentTag($info->getDocument(), 'checkbox', 'dots');
        $arrows = $this->getDocumentTag($info->getDocument(), 'checkbox', 'arrows');
        $speed = $this->getDocumentTag($info->getDocument(), 'select', 'speed');
        $autoplaySpeed = $this->getDocumentTag($info->getDocument(), 'select', 'autoplaySpeed');

        $view = $info->getView();
		$view->distances = $distance->getDistances($this, $info);
		$view->galleryId = $id;
		$view->images = $this->getAssetArray($imagesField->getElements());
        $view->adaptiveHeight = $adaptiveHeight;
        $view->autoplay = $autoplay;
        $view->dots = $dots;
        $view->arrows = $arrows;
        $view->speed = $this->getSpeed($speed);
        $view->autoplaySpeed = $this->getSpeed($autoplaySpeed);
        $view->renderOptions = $renderOption->getRenderOptionClasses($this, $info);
        $view->loading = $loading;
	}

	public function getViewTemplate()
	{
		return "DesignBundle:Areas/designGallery:view." . $this->getTemplateSuffix();
	}

    /**
     * @inheritDoc
     */
    public function getTemplateSuffix()
    {
        return static::TEMPLATE_SUFFIX_TWIG;
    }

	public function getName()
	{
		return "Galerie";
	}

	public function getDescription()
	{
		return "Design Galerie";
	}

	public function getGroupName(): ?string
	{
		return "Design";
	}

    /**
     * @param array $data
     *
     * @return array
     */
    public function getAssetArray($data)
    {
        if (empty($data)) {
            return [];
        }

        $assets = [];

        foreach ($data as $element) {
            if ($element instanceof \Pimcore\Model\Asset\Image) {
                $assets[] = $element;
            } elseif ($element instanceof \Pimcore\Model\Asset\Folder) {
                foreach ($element->getChildren() as $child) {
                    if ($child instanceof \Pimcore\Model\Asset\Image) {
                        $assets[] = $child;
                    }
                }
            }
        }

        return $assets;
    }

    /**
     * Gibt die Geschwindigkeiten für speed und slideSpeed zurück
     */
    protected function getSpeed($speed) {
        $value = "";
        switch($speed) {
            case "slower":
                $value = 7000;
                break;
            case "slow":
                $value = 5000;
                break;
            case "medium":
                $value = 3000;
                break;
            case "fast":
                $value = 1000;
                break;
            case "faster":
                $value = 100;
                break;
        }
        return $value;
    }
}
