<?php

namespace DesignBundle\Document\Areabrick\LinkList;

use DesignBundle\Document\Areabrick\AbstractAreabrick;
use DesignBundle\Model\Distance;
use DesignBundle\Model\RenderOption;
use Pimcore\Model\Document\Tag\Area\Info;

class DesignLinkList extends AbstractAreabrick
{
	public function action(Info $info)
	{
		parent::action($info);

        $flags = $this->configManager->getConfig('flags');
        $useDynamicLinks = $flags['use_dynamic_links'];
		$distance = new Distance();
		$renderOption = new RenderOption();

		$view = $info->getView();
		$view->useDynamicLinks = $useDynamicLinks;
		$view->distances = $distance->getDistances($this, $info);
		$view->renderOptions = $renderOption->getRenderOptionClasses($this, $info);
	}

	public function getViewTemplate()
	{
		return "DesignBundle:Areas/designLinkList:view." . $this->getTemplateSuffix();
	}

    /**
     * @inheritDoc
     */
    public function getTemplateSuffix()
    {
        return static::TEMPLATE_SUFFIX_TWIG;
    }

	public function getName()
	{
		return "Link Liste";
	}

	public function getDescription()
	{
		return "Design Link Liste";
	}

	public function getGroupName(): ?string
	{
		return "Design";
	}

}
