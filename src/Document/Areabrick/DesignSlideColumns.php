<?php

namespace DesignBundle\Document\Areabrick\SlideColumns;

use DesignBundle\Document\Areabrick\AbstractAreabrick;
use DesignBundle\Model\Distance;
use DesignBundle\Model\RenderOption;
use Pimcore\Model\Document\Tag\Area\Info;
use ToolboxBundle\Registry\CalculatorRegistryInterface;

class DesignSlideColumns extends AbstractAreabrick
{
    /**
     * @var CalculatorRegistryInterface
     */
    private $calculatorRegistry;

    /**
     * @param CalculatorRegistryInterface $calculatorRegistry
     */
    public function __construct(CalculatorRegistryInterface $calculatorRegistry)
    {
        $this->calculatorRegistry = $calculatorRegistry;
	}
	
	public function action(Info $info)
	{
		parent::action($info);
		$distance = new Distance();
		$renderOption = new RenderOption();

		/** @var \Pimcore\Model\Document\Tag\Checkbox $equalHeightElement */
        $equalHeightElement = $this->getDocumentTag($info->getDocument(), 'checkbox', 'equal_height');
        $equalHeight = $equalHeightElement->isChecked() && !$info->getView()->get('editmode');

        /** @var Info $brick */
        $brick = $info->getView()->get('brick');
        $id = $brick->getId() . '-' . $brick->getIndex();

        $slidesPerView = (int) $this->getDocumentTag($info->getDocument(), 'select', 'slides_per_view')->getData();
        $slideElements = $this->getDocumentTag($info->getDocument(), 'block', 'slideCols', ['default' => $slidesPerView]);

        $theme = $this->configManager->getConfig('theme');
        $calculator = $this->calculatorRegistry->getSlideColumnCalculator($theme['calculators']['slide_calculator']);

        $slideColumnConfig = $this->getConfigManager()->getAreaParameterConfig('slideColumns');
        $slidesPerViewClass = $calculator->calculateSlideColumnClasses($slidesPerView, $slideColumnConfig);
        $breakpoints = $this->calculateSlideColumnBreakpoints($slidesPerView);

		$view = $info->getView();
		$view->distances = $distance->getDistances($this, $info);
		$view->renderOptions = $renderOption->getRenderOptionClasses($this, $info);
		$view->id = $id;
		$view->slideElements = $slideElements;
		$view->slidesPerView = $slidesPerView;
		$view->slidesPerViewClasses = $slidesPerViewClass;
		$view->breakpoints = $breakpoints;
		$view->equalHeight = $equalHeight;
	}

	public function getViewTemplate()
	{
		return "DesignBundle:Areas/designSlideColumns:view." . $this->getTemplateSuffix();
	}

    /**
     * @inheritDoc
     */
    public function getTemplateSuffix()
    {
        return static::TEMPLATE_SUFFIX_TWIG;
    }

	public function getName()
	{
		return "Slide Columns";
	}

	public function getDescription()
	{
		return "Design Slide Columns";
	}

	public function getGroupName(): ?string
	{
		return "Design";
	}

    /**
     * @param int $columnType
     *
     * @return array
     *
     * @throws \Exception
     */
    private function calculateSlideColumnBreakpoints($columnType)
    {
        $columnType = (int) $columnType;
        $configInfo = $this->getConfigManager()->getAreaParameterConfig('slideColumns');

        $breakpoints = [];

        if (!empty($configInfo)) {
            if (isset($configInfo['breakpoints']) && isset($configInfo['breakpoints'][$columnType])) {
                $breakpoints = $configInfo['breakpoints'][$columnType];
            }
        }

        return $breakpoints;
    }

}
