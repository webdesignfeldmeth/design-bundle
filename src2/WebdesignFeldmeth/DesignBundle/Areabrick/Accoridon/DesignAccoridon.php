<?php

namespace WebdesignFeldmeth\DesignBundle\Document\Areabrick\Accoridon;

use WebdesignFeldmeth\DesignBundle\Document\Areabrick\AbstractAreabrick;
use WebdesignFeldmeth\DesignBundle\Model\Distance;
use WebdesignFeldmeth\DesignBundle\Model\RenderOption;
use Pimcore\Model\Document\Tag\Area\Info;

class DesignAccoridon extends AbstractAreabrick
{
	public function action(Info $info)
	{
		parent::action($info);
		$distance = new Distance();
		$renderOption = new RenderOption();

        $infoParams = $info->getParams();
        if (isset($infoParams['designAccordionId'])) {
            $id = $infoParams['designAccordionId'];
        } else {
            $id = uniqid('accordion-');
        }

		$view = $info->getView();
		$view->id = $id;
		$view->distances = $distance->getDistances($this, $info);
		$view->renderOptions = $renderOption->getRenderOptionClasses($this, $info);
	}

	public function getViewTemplate()
	{
		return "DesignBundle:Areas/designAccoridon:view." . $this->getTemplateSuffix();
	}

    /**
     * @inheritDoc
     */
    public function getTemplateSuffix()
    {
        return static::TEMPLATE_SUFFIX_TWIG;
    }

	public function getName()
	{
		return "Accoridon";
	}

	public function getDescription()
	{
		return "Design Accoridon / Tabs";
	}

	public function getGroupName(): ?string
	{
		return "Design";
	}

}
