<?php

namespace WebdesignFeldmeth\DesignBundle\Document\Areabrick\Image;

use WebdesignFeldmeth\DesignBundle\Document\Areabrick\AbstractAreabrick;
use WebdesignFeldmeth\DesignBundle\Model\Distance;
use WebdesignFeldmeth\DesignBundle\Model\RenderOption;
use Pimcore\Model\Document\Tag\Area\Info;

class DesignImage extends AbstractAreabrick
{
	public function action(Info $info)
	{
		parent::action($info);
		$distance = new Distance();
		$renderOption = new RenderOption();
		$loading = $this->getDocumentTag($info->getDocument(), 'select', 'loading');

		$view = $info->getView();
		$view->distances = $distance->getDistances($this, $info);
		$view->renderOptions = $renderOption->getRenderOptionClasses($this, $info);
		$view->loading = $loading;
	}

	public function getViewTemplate()
	{
		return "DesignBundle:Areas/designImage:view." . $this->getTemplateSuffix();
	}

    /**
     * @inheritDoc
     */
    public function getTemplateSuffix()
    {
        return static::TEMPLATE_SUFFIX_TWIG;
    }

	public function getName()
	{
		return "Bild";
	}

	public function getDescription()
	{
		return "Design Bild";
	}

	public function getGroupName(): ?string
	{
		return "Design";
	}

}
