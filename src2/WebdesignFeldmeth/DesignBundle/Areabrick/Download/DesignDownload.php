<?php

namespace WebdesignFeldmeth\DesignBundle\Document\Areabrick\Download;

use WebdesignFeldmeth\DesignBundle\Document\Areabrick\AbstractAreabrick;
use WebdesignFeldmeth\DesignBundle\Model\Distance;
use WebdesignFeldmeth\DesignBundle\Model\RenderOption;
use Pimcore\Model\Document\Tag\Area\Info;
use ToolboxBundle\Connector\BundleConnector;

class DesignDownload extends AbstractAreabrick
{
	/**
     * @var BundleConnector
     */
    protected $bundleConnector;

    /**
     * Download constructor.
     *
     * @param BundleConnector $bundleConnector
     */
    public function __construct(BundleConnector $bundleConnector)
    {
        $this->bundleConnector = $bundleConnector;
	}
	
	public function action(Info $info)
	{
		parent::action($info);
		$distance = new Distance();
		$renderOption = new RenderOption();

		//check if member extension exist
        $hasMembers = $this->bundleConnector->hasBundle('MembersBundle\MembersBundle');

        /** @var \Pimcore\Model\Document\Tag\Relations $downloadField */
        $downloadField = $this->getDocumentTag($info->getDocument(), 'relations', 'downloads');

        $assets = [];
        if (!$downloadField->isEmpty()) {
            /** @var \Pimcore\Model\Asset $node */
            foreach ($downloadField->getElements() as $node) {
                //it's a folder. get all sub assets
                if ($node instanceof Asset\Folder) {
                    $assetListing = new Asset\Listing();
                    $fullPath = rtrim($node->getFullPath(), '/') . '/';
                    $assetListing->addConditionParam('path LIKE ?', $fullPath . '%');

                    if ($hasMembers) {
                        $assetListing->onCreateQuery(function (QueryBuilder $query) use ($assetListing) {
                            $this->bundleConnector->getBundleService(\MembersBundle\Security\RestrictionQuery::class)
                                ->addRestrictionInjection($query, $assetListing, 'assets.id');
                        });
                    }

                    /** @var Asset $entry */
                    foreach ($assetListing->getAssets() as $entry) {
                        if (!$entry instanceof Asset\Folder) {
                            $assets[] = $entry;
                        }
                    }

                    //default asset
                } else {
                    if ($hasMembers) {
                        /** @var \MembersBundle\Restriction\ElementRestriction $elementRestriction */
                        $elementRestriction = $this->bundleConnector->getBundleService(\MembersBundle\Manager\RestrictionManager::class)->getElementRestrictionStatus($node);
                        if ($elementRestriction->getSection() === \MembersBundle\Manager\RestrictionManager::RESTRICTION_SECTION_ALLOWED) {
                            $assets[] = $node;
                        }
                    } else {
                        $assets[] = $node;
                    }
                }
            }
        }

		$view = $info->getView();
		$view->distances = $distance->getDistances($this, $info);
		$view->downloads = $assets;
	}

	public function getViewTemplate()
	{
		return "DesignBundle:Areas/designDownload:view." . $this->getTemplateSuffix();
	}

    /**
     * @inheritDoc
     */
    public function getTemplateSuffix()
    {
        return static::TEMPLATE_SUFFIX_TWIG;
    }

	public function getName()
	{
		return "Downloads";
	}

	public function getDescription()
	{
		return "Design Downloads";
	}

	public function getGroupName(): ?string
	{
		return "Design";
	}

}
