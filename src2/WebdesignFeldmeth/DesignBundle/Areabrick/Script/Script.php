<?php

namespace WebdesignFeldmeth\DesignBundle\Document\Areabrick\Script;

use WebdesignFeldmeth\DesignBundle\Document\Areabrick\AbstractAreabrick;

use Pimcore\Model\Document\Tag\Area\Info;

/**
 * @author Tobias Feldmeth-Walter <tobias.feldmeth-walter@aufwind-group.de>
 */
class Script extends AbstractAreabrick {

    public function action(Info $info) {
        parent::action($info);

        $scriptToRender = $this->getDocumentTag($info->getDocument(), 'textarea', 'scriptToRender')->getData();
        $script = null;
        
        $view = $info->getView();
        $view->script = $script;
    }

    /**
     * 
     * @return string
     */
    public function getName() {
        return 'Skript Renderer';
    }

    /**
     * 
     * @return string
     */
    public function getId() {
        return 'script';
    }

    /**
     * 
     * @return string
     */
    public function getViewTemplate() {
        return 'DesignBundle:Areas/Script:view.' . $this->getTemplateSuffix();
    }

    /**
    * Gibt den Gruppennamen zurück.
    *
    * @return string|null
    */
    public function getGroupName(): ?string
    {
        return 'StartkLahr';
    }
}
