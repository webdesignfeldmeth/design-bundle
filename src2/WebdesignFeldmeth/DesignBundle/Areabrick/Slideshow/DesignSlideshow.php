<?php

namespace WebdesignFeldmeth\DesignBundle\Document\Areabrick\Slideshow;

use WebdesignFeldmeth\DesignBundle\Document\Areabrick\AbstractAreabrick;
use WebdesignFeldmeth\DesignBundle\Model\Distance;
use WebdesignFeldmeth\DesignBundle\Model\RenderOption;
use Pimcore\Model\Document\Tag\Area\Info;
use WebdesignFeldmeth\FeatureBundle\Model\Feature;

class DesignSlideshow extends AbstractAreabrick
{
	public function action(Info $info)
	{
		parent::action($info);
		$distance = new Distance();
		$renderOption = new RenderOption();

		$theme = $this->getDocumentTag($info->getDocument(), 'select', 'theme');
		$autoplay = $this->getDocumentTag($info->getDocument(), 'checkbox', 'autoplay');
		$autoplaySpeed = $this->getDocumentTag($info->getDocument(), 'numeric', 'autoplaySpeed');
		$dots = $this->getDocumentTag($info->getDocument(), 'checkbox', 'dots');
		$arrows = $this->getDocumentTag($info->getDocument(), 'checkbox', 'arrows');
		$speed = $this->getDocumentTag($info->getDocument(), 'numeric', 'speed');
		$adaptiveHeight = $this->getDocumentTag($info->getDocument(), 'checkbox', 'adaptiveHeight');

		$view = $info->getView();
		$view->distances = $distance->getDistances($this, $info);
		$view->renderOptions = $renderOption->getRenderOptionClasses($this, $info);
		$view->theme = $theme;
		$view->autoplay = $autoplay;
		$view->autoplaySpeed = $autoplaySpeed;
		$view->dots = $dots;
		$view->arrows = $arrows;
		$view->speed = $speed;
		$view->adaptiveHeight = $adaptiveHeight;

	}

	public function getViewTemplate()
	{
		return "DesignBundle:Areas/designSlideshow:view." . $this->getTemplateSuffix();
	}

    /**
     * @inheritDoc
     */
    public function getTemplateSuffix()
    {
        return static::TEMPLATE_SUFFIX_TWIG;
    }

	public function getName()
	{
		return "Slideshow";
	}

	public function getDescription()
	{
		return "Design Slideshow";
	}

	public function getGroupName(): ?string
	{
		return "Design";
	}

}
