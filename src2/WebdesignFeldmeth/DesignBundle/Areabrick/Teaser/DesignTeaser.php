<?php

namespace WebdesignFeldmeth\DesignBundle\Document\Areabrick\Teaser;

use WebdesignFeldmeth\DesignBundle\Document\Areabrick\AbstractAreabrick;
use WebdesignFeldmeth\DesignBundle\Model\Distance;
use WebdesignFeldmeth\DesignBundle\Model\RenderOption;
use Pimcore\Model\Document\Tag\Area\Info;

class DesignTeaser extends AbstractAreabrick
{
	public function action(Info $info)
	{
		parent::action($info);
		$distance = new Distance();
		$renderOption = new RenderOption();

		$flags = $this->configManager->getConfig('flags');
        $useDynamicLinks = $flags['use_dynamic_links'];

		$view = $info->getView();
		$view->distances = $distance->getDistances($this, $info);
		$view->renderOptions = $renderOption->getRenderOptionClasses($this, $info);
		$view->useDynamicLinks = $useDynamicLinks;
	}

	public function getViewTemplate()
	{
		return "DesignBundle:Areas/designTeaser:view." . $this->getTemplateSuffix();
	}

    /**
     * @inheritDoc
     */
    public function getTemplateSuffix()
    {
        return static::TEMPLATE_SUFFIX_TWIG;
    }

	public function getName()
	{
		return "Teaser";
	}

	public function getDescription()
	{
		return "Design Teaser";
	}

	public function getGroupName(): ?string
	{
		return "Design";
	}

}
