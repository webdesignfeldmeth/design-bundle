<?php

namespace WebdesignFeldmeth\DesignBundle\Document\Areabrick\Container;

use WebdesignFeldmeth\DesignBundle\Document\Areabrick\AbstractAreabrick;
use WebdesignFeldmeth\DesignBundle\Model\Distance;
use WebdesignFeldmeth\DesignBundle\Model\RenderOption;
use Pimcore\Model\Document\Tag\Area\Info;

class DesignContainer extends AbstractAreabrick
{
	public function action(Info $info)
	{
		parent::action($info);
		$distance = new Distance();
		$renderOption = new RenderOption();

		$view = $info->getView();
		$view->distances = $distance->getDistances($this, $info);
		$view->renderOptions = $renderOption->getRenderOptionClasses($this, $info);
	}

	public function getViewTemplate()
	{
		return "DesignBundle:Areas/designContainer:view." . $this->getTemplateSuffix();
	}

    /**
     * @inheritDoc
     */
    public function getTemplateSuffix()
    {
        return static::TEMPLATE_SUFFIX_TWIG;
    }

	public function getName()
	{
		return "Container";
	}

	public function getDescription()
	{
		return "Design Container";
	}

	public function getGroupName(): ?string
	{
		return "Design";
	}

}
