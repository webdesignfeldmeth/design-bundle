pimcore.registerNS("pimcore.plugin.DesignBundle");

pimcore.plugin.DesignBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.DesignBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        // alert("DesignBundle ready!");
    }
});

var DesignBundlePlugin = new pimcore.plugin.DesignBundle();
