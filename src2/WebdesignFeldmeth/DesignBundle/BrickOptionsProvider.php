<?php

namespace WebdesignFeldmeth\DesignBundle;

use Pimcore\Model\DataObject\ClassDefinition\Data;
use Pimcore\Model\DataObject\ClassDefinition\DynamicOptionsProvider\SelectOptionsProviderInterface;
use DesignBundle\Document\Areabrick;

class BrickOptionsProvider implements SelectOptionsProviderInterface
{
    /**
     * @param $context array
     * @param $fieldDefinition Data
     * @return array
     */
    public function getOptions($context, $fieldDefinition)
    {
		$dirs = $this->getBricks();

        foreach($dirs as $dir) {
            $result[] = ["key" => $dir, "value" => "design".$dir];
        }

		return $result;
	}
	
	protected function getBricks() {
		$dirs = [];
		foreach(glob("../src/DesignBundle/Document/Areabrick/*") as $dir) {
			if(is_dir($dir))
				$dirs[] = end(explode("/", $dir));
		}
		return $dirs;
	}

    /**
     * Returns the value which is defined in the 'Default value' field  
     * @param $context array
     * @param $fieldDefinition Data
     * @return mixed
     */
    public function getDefaultValue($context, $fieldDefinition)
    {
        return $fieldDefinition->getDefaultValue();
    }

    /**
     * @param $context array
     * @param $fieldDefinition Data
     * @return bool
     */
    public function hasStaticOptions($context, $fieldDefinition)
    {
        return false;
    }
}
