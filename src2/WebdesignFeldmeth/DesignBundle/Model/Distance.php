<?php
namespace WebdesignFeldmeth\DesignBundle\Model;

use Pimcore\Extension\Document\Areabrick\AbstractAreabrick;
use Pimcore\Model\Document\Tag\Area\Info;

class Distance extends AbstractAreabrick {

	public function getDistances($_this, Info $info) {
		$distances = array();
		
		$distances["margin"] = $this->getMargin($_this, $info);
		$distances["padding"] = $this->getPadding($_this, $info);
		
		return $distances;
	}

	public function getMargin($_this, Info $info) {
		$separator = " ";
		$marginTop = $_this->getDocumentTag($info->getDocument(), 'select', 'margin_top');
		$marginRight = $_this->getDocumentTag($info->getDocument(), 'select', 'margin_right');
		if($marginRight->text != null)
			$marginRight = $separator . $marginRight;
		$marginBottom = $_this->getDocumentTag($info->getDocument(), 'select', 'margin_bottom');
		if($marginBottom->text != null)
			$marginBottom = $separator . $marginBottom;
		$marginLeft = $_this->getDocumentTag($info->getDocument(), 'select', 'margin_left');
		if($marginLeft->text != null)
			$marginLeft = $separator . $marginLeft;

		return $marginTop . $marginRight . $marginBottom . $marginLeft;
	}

	public function getPadding($_this, $info) {
		$separator = " ";
		$paddingTop = $_this->getDocumentTag($info->getDocument(), 'select', 'padding_top');
		$paddingRight = $_this->getDocumentTag($info->getDocument(), 'select', 'padding_right');
		if($paddingRight->text != null)
			$paddingRight = $separator . $paddingRight;
		$paddingBottom = $_this->getDocumentTag($info->getDocument(), 'select', 'padding_bottom');
		if($paddingBottom->text != null)
			$paddingBottom = $separator . $paddingBottom;
		$paddingLeft = $_this->getDocumentTag($info->getDocument(), 'select', 'padding_left');
		if($paddingLeft->text != null)
			$paddingLeft = $separator . $paddingLeft;

		return $paddingTop . $paddingRight . $paddingBottom . $paddingLeft;
	}

	public function getTemplateLocation(){}
    public function getTemplateSuffix(){}
    public function getViewTemplate(){}

}