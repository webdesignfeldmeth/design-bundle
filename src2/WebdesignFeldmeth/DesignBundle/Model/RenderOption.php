<?php
namespace WebdesignFeldmeth\DesignBundle\Model;

use Pimcore\Extension\Document\Areabrick\AbstractAreabrick;
use Pimcore\Model\Document\Tag\Area\Info;

use Pimcore\Model\DataObject;

class RenderOption extends AbstractAreabrick {

	/**
	 * Gibt alle Darstellungsoptionen gruppiert nach Ordnern zurück
	 */
	public function getRenderOptions() {
		$renderOptions = new DataObject\RenderOption\Listing();
		$renderOptionList = [];
		foreach($renderOptions as $key => $renderOption) {
			$groupName = $this->getGroupName($renderOption->getParentId());
			$renderOptionList[$groupName][$key]["id"] = $renderOption->getId();
			$renderOptionList[$groupName][$key]["name"] = $renderOption->getName();
			$renderOptionList[$groupName][$key]["class"] = $renderOption->getClassValue();
			$renderOptionList[$groupName][$key]["bricks"] = $renderOption->getBricks();
		}
		return $renderOptionList;
	}

	/**
	 * Gibt den Ordnernamen zurück, nach welchem gruppiert wird
	 */
	protected function getGroupName($parentId) {
		$folderName = \Pimcore\Model\DataObject::getById($parentId);
		$folderName = $folderName->getKey();
		return $folderName;
	}

	public function getRenderOptionClasses($_this, $info) {
		$renderOptions = new DataObject\RenderOption\Listing();
		$renderOptionValues = [];
		foreach($renderOptions as $renderOption) {
			$option = $_this->getDocumentTag($info->getDocument(), 'checkbox', 'render-option-'.$renderOption->getId());
			if($option->value)
				$renderOptionValues[] = $renderOption->getClassValue();
		}
		$classes = implode(" ", $renderOptionValues);
		return $classes;
	}

	public function getTemplateLocation(){}
    public function getTemplateSuffix(){}
    public function getViewTemplate(){}
}