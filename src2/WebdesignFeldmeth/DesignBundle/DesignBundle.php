<?php

namespace WebdesignFeldmeth\DesignBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;

class DesignBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/design/js/pimcore/startup.js'
        ];
    }
}